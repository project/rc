<?php

namespace Drupal\rc_opensocial\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rc_group\Plugin\Block\RcGroupBaseBlock;

/**
 * Provides a 'RcOsGroupBlock' block.
 *
 * @Block(
 *  id = "rc_os_group_block",
 *  admin_label = @Translation("Chat Opensocial group block"),
 * )
 */

class RcOsGroupBlock extends RcGroupBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {

    $config = $this->getConfiguration();

    $form = parent::blockForm($form, $form_state);

    $form['block_options']['button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat button text'),
      '#description' => $this->t('Choose the text that will appear inside the chat button.'),
      '#maxlength' => 255,
      '#size' => 40,
      '#weight' => -1,
      '#default_value' => $config['button_text'] ?? 'Open group chat',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Call the parent class's blockSubmit method
    parent::blockSubmit($form, $form_state);

    // Get the form values
    $values = $form_state->getValues();

    // Save the additional configuration
    $this->configuration['button_text'] = $values['block_options']['button_text'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = parent::build();

    if (empty($build)) {
      return $build;
    }

    $config = $this->getConfiguration();
    $build['#theme'] = 'rc_os_group_block';
    $build['#button_text'] = $config['button_text'];

    return $build;
  }

}
