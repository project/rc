<?php

namespace Drupal\rc_opensocial\Plugin\Block;

use Drupal\rc\Plugin\Block\RcBaseBlock;

/**
 * Provides a 'RcOsUserBlock' block.
 *
 * @Block(
 *  id = "rc_os_user_block",
 *  admin_label = @Translation("Chat Opensocial user block"),
 * )
 */
class RcOsUserBlock extends RcBaseBlock {

  public function build(): array {

    $build = parent::build();

    if (empty($build)) {
      return $build;
    }

    $build['#theme'] = 'rc_os_user_block';

    return $build;
  }

}
