<?php

namespace Drupal\rc_group\Groups;

use ATDev\RocketChat\Groups\Group;

/**
 *
 */
class GroupRc extends Group {

  /**
   * Gets group info.
   *
   * @return \ATDev\RocketChat\Groups\Group|bool
   */
  public function info() {
    static::send("groups.info", "GET", ["roomId" => $this->getGroupId()]);

    if (!static::getSuccess()) {
      static::send("groups.info", "GET", ["roomName" => $this->getGroupId()]);
      if (!static::getSuccess()) {
        return FALSE;
      }
    }

    return $this->updateOutOfResponse(static::getResponse()->group);
  }

}
