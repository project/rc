<?php

namespace Drupal\rc_group\Plugin\Block;


/**
 * Provides a 'RcGroupBlock' block.
 *
 * @Block(
 *  id = "rc_group_block",
 *  admin_label = @Translation("Chat group block"),
 * )
 */
class RcGroupBlock extends RcGroupBaseBlock {

}
