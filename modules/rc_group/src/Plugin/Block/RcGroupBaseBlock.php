<?php

namespace Drupal\rc_group\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\rc\Plugin\Block\RcBaseBlock;
use Drupal\rc\Services\RcUser;
use Drupal\rc_group\Services\RcGroup;
use Drupal\Component\Plugin\Context\ContextInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Abstract Class as a base for RC group blocks.
 */
abstract class RcGroupBaseBlock extends RcBaseBlock {

  /**
   * @var RcGroup
   */

  public RcGroup $rcGroup;

  /**
   * Current path.
   *
   * @var ContextInterface[]|void
   */
  public $routeMatch;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\rc\Services\RcUser $rcUser
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\rc_group\Services\RcGroup $rcGroup
   * @param $routeMatch
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, RcUser $rcUser, EntityTypeManagerInterface $entity_type_manager, RcGroup $rcGroup, $routeMatch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $account, $rcUser, $entity_type_manager);
    $this->rcGroup = $rcGroup;
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('rc.user'),
      $container->get('entity_type.manager'),
      $container->get('rc_group.group'),
      $container->get('current_route_match')
    );

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    // Merge with parent contexts to preserve any other contexts.
    return Cache::mergeContexts(parent::getCacheContexts(), ['user', 'route']);
  }

  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    // Unset the URL as it is determined from the group context.
    unset($form['block_options']['url']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $build['#cache'] = ['max-age' => 0];

    $params = $this->routeMatch->getParameters();
    foreach ($params as $param) {
      if ($param instanceof EntityInterface) {
        $group = $param;
      }
    }


    // Disable block if not on a group entity.
    if (!$group || $group->getEntityTypeId() != 'group' || !$group->field_rc_room_id->value) {
      return $build;
    }

    // Check if the current user is a member in the group.
    if (!$group->getMember($this->getUserById($this->account->id()))) {

      return $build;
    }

    // Get the rocket chat group path.
    $rcGroupInfo = $this->rcGroup->groupInfo($group);

    // Skip if unable to get group info.
    if (!$rcGroupInfo)  {
      return $build;
    }

    // Get the build from parent class.
    $build = parent::build();

    if (empty($build)) {
      return $build;
    }

    // Set the URL to the RC room.
    $roomUrl = '/group/' . $rcGroupInfo->getName();

    // Get the host name to be used with deep linking.
    $serverUrl = $build['#server_url'];
    $host = preg_replace("(^https?://)", "", $serverUrl);

    // Get the block configurations.
    $config = $this->getConfiguration();

    // Update the build keys for the group context.
    $token = $build['#token'];
    $build['#theme'] = 'rc_group_block';
    $build['#group'] = $group->label();
    $build['#url'] = $serverUrl . $roomUrl . ($config['embedded'] ? '?layout=embedded' : '');
    $build['#web'] = $config['visibility_web'] ? 'https://go.rocket.chat/room?host=' . $host . '&resumeToken=' . $token : NULL;
    $build['#mobile'] = $config['visibility_mobile'] ? 'rocketchat://room?host=' . $host . '&resumeToken=' . $token : NULL;

    return $build;
  }


}
