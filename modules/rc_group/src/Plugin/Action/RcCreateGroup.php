<?php

namespace Drupal\rc_group\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\group\Entity\Group;
use Drupal\rc_group\Services\RcGroup;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * @Action(
 *   id = "rc_create_group",
 *   label = @Translation("Create/sync chat group for the selected groups"),
 *   type = "group",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "Manage Rocket Chat settings",
 *   },
 * )
 */
class RcCreateGroup extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\rc_group\Services\RcGroup
   */
  protected RcGroup $rcGroup;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, RcGroup $rcGroup, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->rcGroup = $rcGroup;
    $this->config = $config_factory->get('rc_group.settings');
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('rc_group.group'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {

    if ($entity->getEntityTypeId() !== 'group') {
      return;
    }

    $groupType = $entity->bundle();
    $bundlesConfig = $this->config->get('group_chat_types.types');
    $bundles = array_filter($bundlesConfig);

    if (!array_key_exists($groupType, $bundles)) {
      return;
    }

    // Create or update RC group.

    // If RC room does not exist, create a new one.
    if (!$this->rcGroup->checkIfGroupExists($entity)) {
      $rcRoom = $this->rcGroup->createGroup($entity, TRUE);
    }
    // If RC room exists, validate the ability to link it to the group.
    else {
      $rcRoom = $this->rcGroup->groupInfo($entity);
      $rcRoomId = $rcRoom->getRoomId();
      $groupRoomId = $entity->field_rc_room_id->value;

      // The case where the group has an empty field_rc_room_id.
      if (empty($groupRoomId)) {
        $groupsHasRcRoomId = $this->entityTypeManager
          ->getStorage('group')
          ->loadByProperties(['field_rc_room_id' => $rcRoomId]);

        // Check if there is another group that has the Rc room ID.
        if (empty($groupsHasRcRoomId)) {
          // If not, link the RC room to the group.
          $entity->field_rc_room_id->value = $rcRoom->getRoomId();
          $entity->save();

          // Add the group owner as the RC room owner.
          $this->rcGroup->groupAddOwnerByAdmin($entity->getOwner(), $entity);
        }
        else {
          // If there is another group has the Rc room ID, create a new one.
          $rcRoom = $this->rcGroup->createGroup($entity, TRUE);
        }
      }
    }

    if ($rcRoom) {

      // Add members to room.
      $groupOwner = $entity->getOwner();
      $members = $entity->getMembers();
      $rcModeratorRoles = $this->config->get('groups_moderator_roles');
      $rcOwnerRoles = $this->config->get('groups_admin_roles');

      foreach ($members as $member) {

        $user = $member->getUser();
        if ($user->field_rcid->value) {
          $this->rcGroup->groupInviteUser($groupOwner, $user, $entity);

          // Add RC moderator role to users with permissions.
          if ($this->checkUserGroupRole($member, $groupType, $rcModeratorRoles)) {
            $this->rcGroup->groupAddModerator($groupOwner, $user, $entity);
          }

          // Add RC owner role to users with permissions.
          if ($this->checkUserGroupRole($member, $groupType, $rcOwnerRoles)) {
            $this->rcGroup->groupAddOwner($groupOwner, $user, $entity);
          }
        }

      }

      return $this->t(' chat channel is created for group ' . $entity->label());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {

    if ($object instanceof Group) {
      $access = AccessResult::allowed();
    }
    else {
      $access = AccessResult::forbidden();
    }

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * Checks if a user has a specific role in a group.
   *
   * @param $member
   *   The group member.
   * @param $groupType
   *   The group type.
   * @param $roles
   *   The roles to check.
   *
   * @return bool
   *   TRUE if the user has the role, FALSE otherwise.
   */
  protected function checkUserGroupRole($member, $groupType, $roles) {

    // Add moderator role to users with correct group role.
    if (array_key_exists($groupType, $roles)) {
      $rcGroupModRoles = $roles[$groupType];
      foreach ($rcGroupModRoles as $role => $value) {
        $userGroupRules  =  array_keys($member->getRoles());

        if (in_array($value, $userGroupRules)) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

}
