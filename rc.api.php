<?php

use ATDev\RocketChat\Users\User;
/**
 * @file
 */

/**
 * @param object $entity
 * @param \ATDev\RocketChat\Users\User $rcUser
 *
 *   * Custom hook to provide an interface to map Drupal user fields to Rocket Chat
 *   User fields.
 */
function hook_field_mapping_rc_alter(object $entity, User $rcUser) {
  // Usage example with $entity = Drupal\user\UserInterface $user.
  //  $name = $user->getDisplayName();
  //  $invokeName = $name . ' Invoke';
  //  $rcUser->setName($invokeName);
  //  return $rcUser;.
}

/**
 * @param object $result
 * @param object $user
 */
function hook_rc_create_user(object $result, object $user) {

}

/**
 * @param object $result
 * @param object $user
 */
function hook_rc_update_user(object $result, object $user) {

}
