CONTENTS OF THIS FILE
---------------------

* Introduction
* Submodules
* Installation
* Rocket.Chat server configurations
* Rocket Chat Integration module configurations
* Rocket Chat Group module configurations
* Rocket Chat Social module

INTRODUCTION
------------

The Rocket.Chat integration modules aim to provide a seamless integration with Rocket.Chat open-source communications platform.

SUBMODULES
----------
* The following modules are included:

- Rocket.Chat Integration module
- Rocket.Chat Group module
- Rocket Chat OpenSocial

INSTALLATION
------------

The installation of this module is like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the Rocket.Chat Integration module running
   ```composer require "drupal/rc"```.

2. Without composer, you need to copy/upload the webform
   module to the "modules" directory of your Drupal installation.Also, you need to place the [Rocket Chat Rest API PHP Wrapper Library](https://github.com/alekseykuleshov/rocket-chat) and place it in the vendor directory.

3. Enable the 'Rocket.Chat integration' module and desired sub-modules in 'Extend' (/admin/modules).

4. Set up user permissions. (/admin/people/permissions#module-webform)

ROCKET.CHAT SERVER CONFIGURATIONS
--------------------------
First, you need to have a Rocket.Chat server up and running. for more information and how tos please consult the
Rocket.Chat [official documentation](https://docs.rocket.chat/) and the [installation guide](https://docs.rocket.chat/quick-start/installing-and-updating).

After installing the Rocket.Chat server, make sure to enable the following configurations using your Rocket.Chat admin
account:

- Go to [rocket.chat.server]/admin/Accounts, look for "Two Factor Authentication" and disable it.
- At the same page, look for "Iframe" and enable it.
- Go to [rocket.chat.server]/admin/General, look for "Iframe Integration" and enable "Enable Send" and "Enable Receive". Make sure to use your Drupal domain name in "Send Target Origin" and "Receive Origins". For testing purposes you can use "*" to allow all domains.
- At the same page, look for "REST API" and enable CROS Origin and set your Drupal domain name. For testing purposes you can disable this option.
- Go to [rocket.chat.server]/admin/permissions, search for "token", and enable "Create Personal Access Tokens" and "User Generate Access Token" for the user role.
- Go to [rocket.chat.server]/admin/Accounts, look for "Registration" and disable "Send email to user when user is activated" and "Send email to user when user is deactivated".
- At the same page, look for "Registration Form" and select "disabled".
- (Optionally) you can change the user accounts at [rocket.chat.server]/admin/Accounts to prevent users from changing their usernames, passwords, creating rooms and channels. This highly recommended as the module will take over these operations.
- (Optionally) At the same page, you can disable the registration of new accounts directly which is managed by this module.

ROCKET CHAT INTEGRATION MODULE CONFIGURATION
--------------------------------
**Configuring the Rocket.Chat server credentials**

After completing the Rocket.Chat configurations, go to [YOUR DRUPAL SITE]/admin/rc/settings and insert the credentials:
- **Server URL**: the Rocket.Chat server url with protocol e.g. https://example.rocket.chat
- **Rocket Chat admin username**: The Rocket.Chat username of the Admin
- **Rocket Chat admin password**: The Rocket.Chat password of the admin
Then save.

**Configuring the Chat Accounts configuration**

Go to [YOUR DRUPAL SITE]/admin/rc/user_settings. The configurations are mostly self-explanatory.

**Bulk create/update chat accounts**

This option provides a way to bulk create/update chat accounts that are associated with Drupal users.

Go to [YOUR DRUPAL SITE]/admin/rc/people

**Chat User Blocks**

This module comes with two blocks that uses the rocket.Chat [iFrame integration](https://developer.rocket.chat/rocket.chat/iframe-integration) as the following:

**Chat user block**: That can be placed on any region. It comes with the following configurations:

- iFrame URL: The landing page that you want to view on the Rocket.Chat server.
- iFrame width: the width of the iFrame
- iFrame height: the height of the iFrame
- (Experimental) Show a deep link to open chat app using "https://go.rocket.chat": this option shows a link to open the web or mobile app using [Deep Linking](https://developer.rocket.chat/rocket.chat/deeplink).
- (Experimental) Show a deep link to open chat app using "rocketchat://": Similar to the previous option where it uses "rocketchat://" as a protocol for deep linking.
- Show the embedded version of the chat inside the iFrame: Activates the ["Embedded"](https://developer.rocket.chat/rocket.chat/embedded-layout) version of the Rocket.Chat iFrame.
- New window options: Allows opening the Rocket.Chat iFrame in a new window. This option solves the problem of having persistent chat window.

**Chat user block**: Provides a block with an image or text trigger to open/close the chat block. It comes with the same configurations as the previous block with the following additions:

- Icon image and text: that allows using an image or a text to trigger the chat block.

ROCKET CHAT GROUP MODULE CONFIGURATION
--------------------------------------

**Configuring groups and chat rooms**

Go to [YOUR DRUPAL SITE]/admin/rc/chat_groups_settings and:

- Configure "Group Settings" which are self-explanatory.
- Select the group types that you want to have Rocket Chat channels and SAVE.
- After saving, you will be able to associate the Drupal group type roles with the Rocket chat channels roles and SAVE. for now, the available channels roles are "owner" and "moderator"

**Bulk create/update chat channels**

This option provides a way to bulk create/update chat channels that are associated with Drupal groups.

Go to [YOUR DRUPAL SITE]/admin/rc/rooms

**Chat group Blocs**

This module comes with one block that uses the rocket.Chat [iFrame integration](https://developer.rocket.chat/rocket.chat/iframe-integration):
to present the Group channel on the group page. It comes with the following configurations:

- iFrame width: the width of the iFrame
- iFrame height: the height of the iFrame
- (Experimental) Show a deep link to open chat app using "https://go.rocket.chat": this option shows a link to open the web or mobile app using [Deep Linking](https://developer.rocket.chat/rocket.chat/deeplink).
- (Experimental) Show a deep link to open chat app using "rocketchat://": Similar to the previous option where it uses "rocketchat://" as a protocol for deep linking.
- Show the embedded version of the chat inside the iFrame: Activates the ["Embedded"](https://developer.rocket.chat/rocket.chat/embedded-layout) version of the Rocket.Chat iFrame.
- New window options: Allows opening the Rocket.Chat iFrame in a new window. This option solves the problem of having persistent chat window.

Make sure to adjust the block visibility to be shown on the Group entities only.

ROCKET.CHAT OpenSocial MODULE CONFIGURATION
-------------------------------------------
This module provides an implementation of the module for the OpenSocial distribution. It's an autopilot module that adds
2 blocks with triggering buttons as the following:

- Chat Opensocial user block, where the trigger button is added to the top menu. You can configure the block from the Block Layout admin page.
- Chat Opensocial group block, where the trigger button is added to the "complementary top" region. You can also configure the block from the Block Layout admin page.
