<?php

namespace Drupal\rc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RcUserSettings.
 */
class RcUserSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'rc.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rc_user_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Getting the stored values of the form.
    $config = $this->config('rc.settings');

    $form['create_user_register'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically create a new chat account when new user is registered.'),
      '#default_value' => ($config->get('user.create_user_register')) ?? TRUE,
    ];

    $form['create_user_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create or update Chat account when a user logs in'),
      '#default_value' => ($config->get('user.create_user_login')) ?? TRUE,
    ];

    $form['create_user_edit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new Chat account when a user edits the account'),
      '#default_value' => $config->get('user.create_user_edit') ?? TRUE,
    ];

    $form['add_admin_role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add chat admin to users with administrator role'),
      '#default_value' => $config->get('user.add_admin_role') ?? TRUE,
    ];

    $form['create_user_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create chat accounts while running maintenance tasks.'),
      '#description' => $this->t('Use this option to allow creating chat accounts while running cron job.'),
      '#default_value' => ($config->get('user.create_user_cron')) ?? TRUE,
    ];

    $form['update_user_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update chat accounts while running maintenance tasks.'),
      '#description' => $this->t('Use this option to allow updating chat accounts while running cron job.'),
      '#default_value' => ($config->get('user.update_user_cron')) ?? TRUE,
    ];

    $form['visibility_rcid_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the Chat account ID and Token fields at the user edit form'),
      '#default_value' => ($config->get('user.visibility_rcid_form')) ?? FALSE,
    ];

    $form['visibility_rcid_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the Chat account ID and Token fields at the user "display" page'),
      '#default_value' => ($config->get('user.visibility_rcid_display')) ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('rc.settings')
      ->set('user.create_user_register', $form_state->getValue('create_user_register'))
      ->set('user.create_user_login', $form_state->getValue('create_user_login'))
      ->set('user.create_user_edit', $form_state->getValue('create_user_edit'))
      ->set('user.add_admin_role', $form_state->getValue('add_admin_role'))
      ->set('user.create_user_cron', $form_state->getValue('create_user_cron'))
      ->set('user.update_user_cron', $form_state->getValue('update_user_cron'))
      ->set('user.visibility_rcid_form', $form_state->getValue('visibility_rcid_form'))
      ->set('user.visibility_rcid_display', $form_state->getValue('visibility_rcid_display'))
      ->save();
  }

}
