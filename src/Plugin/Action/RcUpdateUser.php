<?php

namespace Drupal\rc\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\rc\Services\RcUser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Some description.
 *
 * @Action(
 *   id = "rc_update_user",
 *   label = @Translation("Update chat accounts for the selected users"),
 *   type = "user",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "Manage Rocket Chat settings",
 *   },
 * )
 */
class RcUpdateUser extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * The Rocket Chat user service.
   *
   * @var \Drupal\rc\Services\RcUser
   */
  protected RcUser $rcUser;

  /**
   * Constructs a new RcUpdateUser object.
   *
   * @param \Drupal\rc\Services\RcUser $rcUser
   *   The Rocket Chat user service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RcUser $rcUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->rcUser = $rcUser;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('rc.user')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity->getEntityTypeId() == 'user') {
      $updateUser = $this->rcUser->updateUser($entity, TRUE);
      if ($updateUser) {
        return $this->t($updateUser->getUsername() . ' chat account is updated for user ' . $entity->getDisplayName());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'user') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    return AccessResult::allowed();
  }


}
