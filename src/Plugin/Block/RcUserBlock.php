<?php

namespace Drupal\rc\Plugin\Block;


/**
 * Provides a 'RcUserBlock' block.
 *
 * @Block(
 *  id = "rc_user_block",
 *  admin_label = @Translation("Chat user block"),
 * )
 */
class RcUserBlock extends RcBaseBlock {

}
