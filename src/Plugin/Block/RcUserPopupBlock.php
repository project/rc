<?php

namespace Drupal\rc\Plugin\Block;


use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a 'RcUserBlock' block.
 *
 * @Block(
 *  id = "rc_user_popup_block",
 *  admin_label = @Translation("Chat user popup block"),
 * )
 */
class RcUserPopupBlock extends RcBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {

    $defaultConfig = parent::defaultConfiguration();
    $defaultConfig['button_element'] = 'text';
    $defaultConfig['button_text'] = 'Chat';

    return $defaultConfig;

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['button_options'] = [
      '#type' => 'details',
      '#title' => t('Icon image and text'),
      '#open' => TRUE,
    ];

    $form['button_options']['button_element'] = [
      '#type' => 'select',
      '#title' => t('Select the button element'),
      '#options' => [
        'icon' => t('Use image icon'),
        'text' => t('Use text'),
      ],
      '#default_value' => $config['button_element'] ?? 'text',
      '#attributes' => [
        'id' => 'rc-button-config',
      ],
    ];

    $form['button_options']['icon_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[id="rc-button-config"]' => ['value' => 'icon'],
        ],
        'required' => [
          ':input[id="rc-button-config"]' => ['value' => 'icon'],
        ],
      ],
    ];

    $form['button_options']['icon_container']['button_icon'] = [
      '#type' => 'managed_file',
      '#title' => t('Icon button'),
      '#name' => 'button_icon',
      '#default_value' => $config['button_icon'],
      '#upload_location' => 'public://rc_chat_icons',
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
      ],
      '#states' => [
        'required' => [
          ':input[id="rc-button-config"]' => ['value' => 'icon'],
        ],
      ],
    ];

    $form['button_options']['button_text'] = [
      '#type' => 'textfield',
      '#title' => t('text Button'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config['button_text'] ?? 'Chat',
      '#states' => [
        'visible' => [
          ':input[id="rc-button-config"]' => ['value' => 'text'],
        ],
        'required' => [
          ':input[id="rc-button-config"]' => ['value' => 'text'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    // Generating the icon image URL to be saved in the block config.
    $iconId = $values['button_options']['icon_container']['button_icon'][0];
    $iconUrl = NULL;
    if (isset($iconId)) {
      $iconUrl = File::load($iconId)->createFileUrl();
    }

    $this->configuration['button_element'] = $values['button_options']['button_element'];
    $this->configuration['button_text'] = strip_tags($values['button_options']['button_text']);
    $this->configuration['button_icon'] = $values['button_options']['icon_container']['button_icon'];
    $this->configuration['button_icon_path'] = $iconUrl;


  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    $build = parent::build();
    if (empty($build)) {
      return $build;
    }

    $config = $this->getConfiguration();

    $build['#theme'] = 'rc_user_popup_block';
    $build['#button_element'] = $config['button_element'];
    $build['#button_icon'] = $config['button_icon'];
    $build['#button_icon_path'] = $config['button_icon_path'];
    $build['#button_text'] = $config['button_text'];

    return $build;

  }

}
