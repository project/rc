<?php

namespace Drupal\rc\Services;

use ATDev\RocketChat\Chat;

/**
 *
 */
class RcChat extends chat {

  /**
   * Return  user authToken with provided credentials.
   *
   * @param string $userName
   * @param string $password
   * @param bool $auth
   *
   * @return \ATDev\RocketChat\Users\User|bool
   */
  public static function authToken($userName, $password, $auth = TRUE) {
    static::send("login", "POST", ["user" => $userName, "password" => $password]);

    // Own error structure.
    if (isset(static::getResponse()->status) && (static::getResponse()->status != "success")) {

      if (isset(static::getResponse()->error)) {
        static::setError(static::getResponse()->error);
      }
      else {
        static::setError("Unknown error occurred while logging in");
      }

      return FALSE;
    }

    return static::getResponse()->data->authToken;
  }

}
