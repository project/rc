<?php

namespace Drupal\rc\Services;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class RcAuth.
 */
class RcAuth {

  /**
   * The module configuration.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * @param Drupal\Core\Config\ConfigFactory
   */
  public function __construct(ConfigFactory $config) {
    $this->config = $config->get('rc.settings');
  }

  /**
   * Get the Rocket Server URL.
   *
   * @return array|mixed|null
   */
  public function url() {
    return $this->config->get('access.server');
  }

  /**
   * Get the Rocket Chat admin username.
   *
   * @return array|mixed|null
   */
  public function user() {
    return $this->config->get('access.user');
  }

  /**
   * Get the Rocket Chat admin password.
   *
   * @return array|mixed|null
   */
  public function secret() {
    return $this->config->get('access.secret');
  }

}
